//
//  ViewController.swift
//  Water Droplet
//
//  Created by innovapost on 2018-03-02.
//  Copyright © 2018 innovapost. All rights reserved.
//

import Foundation
import SceneKit

class ViewControllerAuto: UIViewController{
  
  @IBOutlet weak var seconds: UILabel!
  @IBOutlet weak var sceneView: SCNView!
  override func viewDidLoad() {
    super.viewDidLoad()
    let scene = SCNScene(named: "droplet-splash.scn")!
    
    let node = scene.rootNode.childNode(withName: "rain", recursively: false)
    
    
    let splash = SCNParticleSystem(named: "plok", inDirectory: nil)!
    splash.idleDuration = 0
    splash.loops = false

    let rain = SCNParticleSystem(named: "rain", inDirectory: nil)!
    rain.systemSpawnedOnCollision = splash
    node?.addParticleSystem(rain)
    
    let floor = scene.rootNode.childNode(withName: "floor", recursively: false)!
    rain.colliderNodes = [floor]
    
    self.sceneView.scene = scene
  }
}

/* SceneKit File Editor notes:
 1. Scene Inspector, I have to change gravity to -9.8 instead of -1 so that I can have the falling down of the rain drops. Without that change the drops werer too slow.
 2. Floor node: I have to move it down by -5 in y axis so that the collision happens at the bottom of the screen and it can be seen by the camera (this also helps in seeing plok particle system from top a little bit to have the normal splash effect). Without doing so, I got another nice animation as if the drops are spalshing and falling towards me (the camera lense). I might use this trick to fill in the milk cup ;)
 3. Floor node: Material Inspector: I have to change the Diffuse color to match the scene background color so that the user doesn't see the borders of the floor plane. Also I have to change the light model to be Constant instead of Blinn
 */
